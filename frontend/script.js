async function addData() {
    const key1 = document.getElementById('key1').value;
    const key2 = document.getElementById('key2').value;
    const key3 = document.getElementById('key3').value;

    try {
        const response = await fetch('/api/data', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ key1, key2, key3 }),
        });

        const result = await response.json();
        displayResult(result.output);
    } catch (error) {
        console.error('Error adding data:', error);
        displayResult('Error adding data. Please try again.', 'error');
    }
}

async function deleteData() {
    const uniqueId = document.getElementById('uniqueId').value;

    try {
        const response = await fetch(`/api/data/${uniqueId}`, {
            method: 'DELETE',
        });

        const result = await response.json();
        displayResult(result.message);
    } catch (error) {
        console.error('Error deleting data:', error);
        displayResult('Error deleting data. Please try again.', 'error');
    }
}

function displayResult(message, type = 'success') {
    const resultDiv = document.getElementById('result');
    resultDiv.textContent = message;
    resultDiv.style.backgroundColor = type === 'success' ? '#e0f7fa' : '#ffcdd2';
    resultDiv.style.display = 'block';

    setTimeout(() => {
        resultDiv.style.display = 'none';
    }, 5000);
}
