from sanic import Sanic, response
#from sanic.exceptions import abort
import json
import asyncpg
import subprocess

app = Sanic(__name__)

# PostgreSQL database configuration
DB_CONFIG = {
    'user': 'your_db_user',
    'password': 'your_db_password',
    'database': 'your_db_name',
    'host': 'your_db_host',
}

# Route for GET request
@app.route('/api/info', methods=['GET'])
async def get_info(request):
    app_info = {
        'name': 'Your App',
        'version': '1.0',
        'description': 'Your App Description',
    }
    return response.json(app_info)

# Route for POST request
@app.route('/api/data', methods=['POST'])
async def post_data(request):
    try:
        user_input = request.json
        # Save user input to file
        with open('user_input.json', 'w') as f:
            json.dump(user_input, f)
        
        # Insert user input into PostgreSQL database
        conn = await asyncpg.connect(**DB_CONFIG)
        await conn.execute('''
            INSERT INTO user_input_details (key1, key2, key3)
            VALUES ($1, $2, $3)
        ''', user_input['key1'], user_input['key2'], user_input['key3'])
        await conn.close()

        # Trigger Python script and get output
        script_output = subprocess.check_output(['python', 'your_script.py', 'user_input.json'])
        
        # Return script output as JSON
        return response.json({'result': 'success', 'output': script_output.decode()})
    
    except Exception as e:
        print(e)
        #abort(500, 'Internal Server Error')

# Route for DELETE request
@app.route('/api/data/<unique_id>', methods=['DELETE'])
async def delete_data(request, unique_id):
    try:
        # Delete entry from PostgreSQL database
        conn = await asyncpg.connect(**DB_CONFIG)
        await conn.execute('DELETE FROM user_input_details WHERE unique_id = $1', unique_id)
        await conn.close()

        # Trigger Python script and get message
        script_output = subprocess.check_output(['python', 'your_script.py', 'delete', unique_id])

        return response.json({'result': 'success', 'message': script_output.decode()})
    
    except Exception as e:
        print(e)
        #abort(500, 'Internal Server Error')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
