import os
import logging
import tempfile
from sanic import Sanic, response, redirect
from sanic.request import Request
import json, secrets, string
from datetime import datetime
import asyncpg
from sanic_cors import CORS
from sanic_session import Session
from sanic_jinja2 import SanicJinja2
from scripts import get_script, post_script, delete_script

app = Sanic(__name__)
CORS(app)
jinja = SanicJinja2(app)
html_dir = os.path.join(os.path.dirname(__file__), 'static')
log_dir = os.path.join(os.path.dirname(__file__), 'app_logs')

if not os.path.exists(log_dir):
    os.mkdir(log_dir)
log_file = os.path.join(log_dir, 'app.log')
logging.basicConfig(filename=log_file, level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def uniqid():
    current_datetime = datetime.now()
    formatted_datetime = current_datetime.strftime("%H%M%S%f-%d%m-%Y")
    characters = string.ascii_letters + string.digits
    extraid = ''.join(secrets.choice(characters) for _ in range(4))
    uniq_resource_id = f"{formatted_datetime}-{extraid}"
    return uniq_resource_id
uniq_resource_id = uniqid()

def create_temp_json_file(data):
    temp_dir = tempfile.mkdtemp()
    temp_file_path = os.path.join(temp_dir, 'user_input.json')

    with open(temp_file_path, 'w') as file:
        json.dump(data, file)

async def insert_user_input(data):
    conn = await asyncpg.connect(
        user='postgres',
        password='abc123',
        databse='mydb',
        host='localhost',
        port=5432
    )
    resource_id = uniq_resource_id
    try:
        await conn.execute(
            'INSERT INTO user_input_data (username, useremail, usercity, uniq_id)'
            'VALUES ($1, $2, $3)',
            data['username'], data['useremail'], data['usercity'], resource_id'
        )
    except Exception as e:
        logging.error(f'Failed to insert user input into postgres DB: {str(e)}')
        return f'Failed to insert user input into postgres DB: {str(e)}'
    finally:
        await conn.close()

async def query_and_delete_user_input(data):
    conn = await asyncpg.connect(
        user='postgres',
        password='abc123',
        databse='mydb',
        host='localhost',
        port=5432
    )
    try:
        query = 'SELECT * FROM user_input_data WHERE '
        conditions = [f'{key} = ${i+1}' for i, (key, value) in enumerate(data.items())]
        query += ' AND '.join(conditions)
        results = await conn.fetch(query, *data.values())

        if results:
            await conn.execute('DELETE FROM user_input_data WHERE ' + ' AND '.join(conditions), *data.values())
            return results
        else:
            return 'No Matching entries found in DB'
    except Exception as e:
        logging.error(f'Failed to query and delete user input from postgressql DB: {str(e)}')
        return f'Error: {str(e)}'
    finally:
        await conn.close()

app.static('/static', './static')

@app.route('/api/adduser', methods=['POST'])
async def post_endpoint(request):
    try:
        data = request.json
        if not data:
            return response.text('Error: Invalid JSON data passed', status=400)
        json_file_path = create_temp_json_file(data)
        db_response = await insert_user_input(data)
        if db_response is not None and db_response.startswith("Failed"):
            logging.error(f'DB insertion failed')
            return response.text(f'Error: DB insertion failed {db_response}', status=500)
        logging.info(f'POST endpoint called {uniq_resource_id}')
        result = post_script.run_post_script(json_file_path, uniq_resource_id)

        return response.json(result)
    except Exception as e:
        logging.error(f'DB insertion failed: {e}')
        return response.text('Error: DB insertion failed', status=500)
    
@app.route('/api/deleteuser', methods=['DELETE'])
async def delete_endpoint(request):
    data = request.json
    if not data:
        logging.error('DELETE endpoint received invalid JSON data')
        return response.text('DELETE endpoint received invalid JSON data', status=400)
    json_file_path = create_temp_json_file(data)

    logging.info('DELETE endpoint called')
    result = delete_script.run_delete_script(json_file_path)

    return response.text(result)

@app.route('/api/info', methods=['GET'])
async def get_endpoint(request):
    logging.info('GET endpoint caalled')
    result = get_script.run_get_script()

    return response.json(result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)